#ifndef COMMON_H
#define COMMON_H

#include<stdint.h>
#include<string.h>

void stoiIP(const char *in_s, uint8_t *out_ip);
void lower_to_upper_mac(uint8_t * addr_mac);


#endif // COMMON_H