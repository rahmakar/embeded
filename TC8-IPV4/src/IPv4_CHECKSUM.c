#include "AbstractionAPI.h"
#include "IPv4_CHECKSUM.h"
#include <stdio.h>
#include <string.h>

int IPv4_CHECKSUM_02()
{
    // 1. TESTER: Send an ICMPv4 Echo Request with Header Checksum indicating <invalidChecksum>
    ICMP_Packet ICMP_P = CreateICMP();
    ICMP_Compute_checksum(&ICMP_P);
    EditICMPField(&ICMP_P, ICMP, ICMP_checksum, (void *)0x6d00);
    SendICMP(ICMP_P);

    // 2. TESTER: Listen for a response
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));

    ICMP_Packet ICMP_PR = ListenICMP(filter, 3);

    // 3. DUT: Does not send an ICMPv4 Echo Reply
    if(ICMP_PR.length == 0)
    {
        printf("IPv4_CHECKSUM_02: Test passed\n");
    }
    else
    {
        printf("IPv4_CHECKSUM_02: Test failed -> DUT sent a response\n");
    }
}

int IPv4_CHECKSUM_05() 
{
    // 1. TESTER: Send an ICMP Echo Request
    ICMP_Packet ICMP_P = CreateICMP();
    ICMP_Compute_checksum(&ICMP_P);
    SendICMP(ICMP_P);

    // 2. TESTER: wait for a reply
    Packet_filter filter;
    strcpy(filter.dstAdd, (char*)GetICMPField(&ICMP_P, IP, IP_SrcAddress));
    strcpy(filter.SrcAdd, (char*)GetICMPField(&ICMP_P, IP, IP_DstAddress));

    ICMP_Packet ICMP_PR1 = ListenICMP(filter, 3);

    // 3. TESTER: Verify that the received ICMP Echo Reply contains: - IP Checksum field to "16 bit one's complement of the one's complement sum of all 16 bit words in the header"
    ICMP_Packet Check_ICMP_PR = ICMP_PR1;
    EditICMPField(&Check_ICMP_PR, IP, IP_HeaderChecksum, (void *)0x6d00);
    ICMP_Compute_checksum(&Check_ICMP_PR);
    if((int)GetICMPField(&ICMP_PR1, IP, IP_HeaderChecksum) == (int)GetICMPField(&Check_ICMP_PR, IP, IP_HeaderChecksum))
    {
        printf("IPv4_CHECKSUM_05: Test passed\n");
        return 0;
    }
    else
    {
        printf("IPv4_CHECKSUM_05: Test failed -> bad checksum\n");
        return 1;
    }
}
