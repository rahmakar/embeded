#ifndef  IPV4COMMON_H
#define IPV4COMMON_H

#ifdef __cplusplus
extern "C"
{
#endif  /* #ifdef __cplusplus*/

/**************************************************************************************************/
/*                                                                                                */
/*                                           Defines                                              */
/*                                                                                                */
/**************************************************************************************************/

#define IP_INI_REASSEMBLE_TIMEOUT   30
#define FRAGMENT_REASSEMBLY_TIMEOUT 15

#define LOOPBACK_ADDRESS "127.0.0.1"

#ifdef __cplusplus
}
#endif  /* #ifdef __cplusplus*/
#endif
