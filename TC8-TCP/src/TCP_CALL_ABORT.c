#include "AbstractionAPI.h"
#include "TCPcommon.h"
#include "TCP_CALL_ABORT.h"


#include"TestabilityProtocol_api.h"

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CALL_ABORT_02
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_CALL_ABORT_02 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CALL_ABORT_02()
{
    int socketID;

    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to ESTABLISHED state 
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Cause the application to issue an ABORT call
    TP_TcpCloseSocket(NULL, (uint16_t)socketID, TRUE);

    //3. DUT: Send a RST control message
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;

    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    { 
        int rRST  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (rRST == 1)
        {
           //result ok
        } 
        else
        {
            printf ("\nTest failed --> DUT response is not RST\n");
        }
    }
    else 
    {
	    TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT moves on to CLOSED state
    if(checkState(CLOSED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in CLOSED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

int TP_TcpCloseSocket_cb_result;
void TP_TcpCloseSocket_cb(TP_ResultID_t result)
{
    TP_TcpCloseSocket_cb_result = result;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CALL_ABORT_03_IT01
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_CALL_ABORT_03 test first iteration.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CALL_ABORT_03_IT01()
{

    int socketID;

    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to the <wst> state
    uint32 seqN, ackN;    
    moveDUTToClosing(&socketID, TCPConfig.DUT_Port , TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Cause the application on the DUT-side to issue an ABORT call
    TP_TcpCloseSocket(TP_TcpCloseSocket_cb, (uint16_t)socketID, true);

    //3. DUT: Issues an ABORT call
    //4. TESTER: Verify that the DUT responds with \"ok\" to the application
    if (TP_TcpCloseSocket_cb_result =! RID_E_OK)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed\n");
		return 1; 
    }


    //5. TESTER: Verify that the DUT moves on to CLOSED state
    if(checkState(CLOSED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in CLOSED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }

}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CALL_ABORT_03_IT02
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_CALL_ABORT_03 test second iteration.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CALL_ABORT_03_IT02()
{

    int socketID;

    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to the <wst> state
    uint32 seqN, ackN;    
    moveDUTToLASTACK(&socketID, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Cause the application on the DUT-side to issue an ABORT call
    TP_TcpCloseSocket(TP_TcpCloseSocket_cb, (uint16_t)socketID, true);

    //3. DUT: Issues an ABORT call
    //4. TESTER: Verify that the DUT responds with \"ok\" to the application
    if ( TP_TcpCloseSocket_cb_result =! RID_E_OK)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed\n");
		return 1; 
    }


    //5. TESTER: Verify that the DUT moves on to CLOSED state
    if(checkState(CLOSED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in CLOSED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }

}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CALL_ABORT_03_IT03
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_CALL_ABORT_03 test third iteration.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CALL_ABORT_03_IT03()
{

    int socketID;

    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to the <wst> state
    uint32 seqN, ackN;    
    moveDUTToTimeWait(&socketID, TCPConfig.DUT_Port , TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Cause the application on the DUT-side to issue an ABORT call
    TP_TcpCloseSocket(TP_TcpCloseSocket_cb, (uint16_t)socketID, true);

    //3. DUT: Issues an ABORT call
    //4. TESTER: Verify that the DUT responds with \"ok\" to the application
    if (TP_TcpCloseSocket_cb_result =! RID_E_OK)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed\n");
		return 1; 
    }


    //5. TESTER: Verify that the DUT moves on to CLOSED state
    if(checkState(CLOSED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in CLOSED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }

}
   

