#include "AbstractionAPI.h"
#include "TCPcommon.h"
#include "TCP_ACKNOWLEDGEMENT.h"

#include"TestabilityProtocol_api.h"

int TCP_ACKNOWLEDGEMENT_02()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Bring DUT to ‘ESTABLISHED’ state.
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketID, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send <sendTCPPacket> on the <udpPort> to request a TCP packet from the DUT.
    vint8 data;
    data.Data=(uint8*)"data";
    data.dataLength=4;
    TP_TcpSendData(NULL, socketID, 9,  0, data);

    //3. DUT: Sends a TCP packet with the expected Sequence Number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    { 
        int rSeqNum  = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if (rSeqNum == ackN+1)
        {
            // result ok 
        } 
        else
        {
            printf ("\nTest failed\n");
            TP_GenaralEndTest(NULL,0,(text){0});

            return 1;

        }
    }
    else 
    {
	TP_GenaralEndTest(NULL,0,(text){0});
     
    printf("\nTest failed --> DUT not responding\n");
    return 1;
    }


    //4. TESTER: Send an ACK with a <payload>.
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 2));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"Data to send");

    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);




    //5. DUT: Accepts the TCP packet and sends an ACK with the expected Akc Number
    TCP_Packet TCP_RP2 = ListenTCP(f);
    if (TCP_RP2.length != 0) 
    { 
        int rACKNum  = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if (rACKNum == ackN+1)
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest passed \n");
            return 0;
        } 
        else
        {
           
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> DUT not ok\n");
            return 1;
        }
    }
    else 
    {
	TP_GenaralEndTest(NULL,0,(text){0});
     
    printf("\nTest failed --> DUT not responding\n");
    return 1;
    }

}

int TCP_ACKNOWLEDGEMENT_03()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Bring DUT to ‘ESTABLISHED’ state using a passive socket call. 
    uint32 seqN, ackN;
    moveClientDUTToEstablished(&socketID, TCPConfig.DUT_Port , TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send TCP packet
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"Data to send");
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Sends an ACK with the expected Ack Number.

    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    { 
        int rAckNum  = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        if (rAckNum == ackN+2)
        {
            printf ("\nTest passed\n");
            TP_GenaralEndTest(NULL,0,(text){0});

            return 0;  
        } 
        else
        {
            printf ("\nTest failed\n");
            TP_GenaralEndTest(NULL,0,(text){0});

            return 1;

        }
    }
    else 
    {
	TP_GenaralEndTest(NULL,0,(text){0});
     
    printf("\nTest failed --> DUT not responding\n");
    return 1;
    }

 

}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_ACKNOWLEDGEMENT_04
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_ACKNOWLEDGEMENT_04 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_ACKNOWLEDGEMENT_04()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Bring DUT to ‘ESTABLISHED’ state. 
    uint32 seqN, ackN;
    moveClientDUTToEstablished(&socketID, TCPConfig.DUT_Port , TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send <sendTCPPacket> to request a TCP packet from the DUT.
    vint8 data;
    data.Data = (uint8*) "TCP From DUT";
    data.dataLength = 4;


    TP_TcpSendData(NULL, socketID, 0,  0, data);

    //3. DUT: Sends a TCP packet.
    int acknumber, sequenceNumber;
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    { 
        acknumber = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber); 
        sequenceNumber = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
    }
    else 
    {
        printf("\nTest failed --> DUT  responding\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }

    //4. TESTER: Send an ACK with the expected Ack Number, with Length equal to 0.
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(acknumber + 1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)sequenceNumber);
    EditTCPField(&TCP_P, TCP, TCP_HeaderLength, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"Data to send");
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //5. DUT: Sends no RST and ends the connection correctly when requested to. 

    TCP_Packet TCP_RP2 = ListenTCP(f);
    if (TCP_RP2.length == 0) 
    { 
        printf ("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;  
    }
    else 
    {
        printf("\nTest failed --> DUT  responding\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }



}

