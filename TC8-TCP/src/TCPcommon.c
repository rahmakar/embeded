#include "AbstractionAPI.h"
#include"TCPcommon.h"

#include"TestabilityProtocol_api.h"

TCP_config_t TCPConfig;

int socketId = 0;
void CreateAndBind_cb(TP_ResultID_t b, uint16 sock)
{
    socketId = sock;
}

uint8 state = 0;
void TP_TcpGetState_cb(TP_ResultID_t b, uint8 s)
{
    state = s;
}

void stoiIP (const char *in_s, uint8 *out_ip) 
{
    char temp_c;
    uint8 octet_i = 0;
    uint8 octet_p = 0;;
    for (uint8 i = 0; i < strlen(in_s); i++ ) 
    {
        temp_c = in_s[i];
        if (temp_c == '.') 
        {
            out_ip[octet_p] = octet_i;
            octet_i = 0;
            octet_p += 1;
        } 
        else 
        {
            octet_i *= 10;
            octet_i += temp_c - '0';
        }
    }
    out_ip[octet_p] = octet_i;
}

bool checkState(tcp_state st)
{
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    if(st != LISTEN)
    {
      stoiIP(TCPConfig.TESTER_IP, ipTESTER.Data);
      TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    }
    else
    {
      stoiIP("0.0.0.0", ipTESTER.Data);
      TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, 0, ipTESTER);
    }
    sleep(3);
    printf("\n****************************** Verifying state | state = %d ******************************\n", state);
    if(state == st)
    {
      printf("\n****************************** :D ******************************\n");
		  return true;
    }
    else
    {
      printf("\n****************************** :'( ******************************\n");
		  return false;  

    }
}

bool moveDUTToListen(int* socketDUT, uint16 portDUT, uint16 maxCon) 
{
    ip4addr ipv4;
    ipv4.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipv4.Data);
    // Move DUT to listen state
    TP_TcpCreateAndBind(CreateAndBind_cb, true, portDUT, ipv4);
    sleep(3);
    *socketDUT = socketId;
    TP_TcpListenAndAccept(NULL, NULL, socketId, maxCon);
    sleep(3);

    // Verify that the DUT  in LISTEN state
    if(checkState(LISTEN))
    {
        return 0;
    }
    else{
        return 1;
    }
}

bool moveDUTToSYNRCVD(int* socketDUT, uint16 portDUT, uint16 maxCon, uint32* seqN, uint32* ackN) 
{
    // Move DUT to Listen state
    moveDUTToListen(socketDUT, portDUT, maxCon);

    // Sending SYN to DUT
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)portDUT);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x05);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = portDUT;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    {
        uint8 rSYN  = (uint8)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        uint8 rACK  = (uint8)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSYN == 1) && (rACK == 1))
        {
            if (seqN != NULL) *seqN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
            if (ackN != NULL) *ackN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        }
    }

    // Verify that the DUT in SYN-RCVD state
    if(checkState(SYNRCVD))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

bool moveServDUTToEstablished(int* socketDUT, uint16 portDUT, uint16 maxCon, uint32* seqN, uint32* ackN) 
{
    // Move DUT to listen state
    moveDUTToListen(socketDUT, portDUT, maxCon);

    // Sending SYN to DUT
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)portDUT);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x05);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = portDUT;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    {
        int rSEQNumber = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)0);
        EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
        EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x06);
        EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(rSEQNumber + 1));
        TCP_Compute_checksum(&TCP_P);
        SendTCP(TCP_P);
        if (seqN != NULL) *seqN = (uint32)GetTCPField(&TCP_P, TCP, TCP_SeqNumber);
        if (ackN != NULL) *ackN = (uint32)GetTCPField(&TCP_P, TCP, TCP_AckNumber);     
    }

    // Verify that the DUT in Established state
    if(checkState(ESTABLISHED))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

bool moveClientDUTToEstablished(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN) 
{
    // Move DUT to SYN-SENT
    moveDUTToSYNSENT(socketDUT, portDUT, portTESTER, maxCon, seqN, ackN);

    //send SYN,ACK
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(*ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(*seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //Receive ACK
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;

    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        printf("\n ACK packet received %s : %d \n", __FILE__, __LINE__);
        int rACK  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1) 
        {
            if (seqN != NULL) *seqN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
            if (ackN != NULL) *ackN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        } 
        else 
        {
            return 1;
        }
    } 
    else 
    {
        printf("\n No packet received %s : %d \n", __FILE__, __LINE__);
        return 1;

    }

    // Verify that the DUT in Established state
    if(checkState(ESTABLISHED))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

bool moveDUTToFINWAIT1(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN)
{
    // Move DUT to ESTABLISHED state   
    moveClientDUTToEstablished(socketDUT, portDUT, portTESTER, maxCon, seqN, ackN);

    // Cause DUT to issue a close() call
    TP_TcpCloseSocket(NULL, (uint16_t)*socketDUT, FALSE);
    sleep(3);

    // Receive FIN
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;

    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    {
        int rFIN  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagFIN);
        if (rFIN == 1) 
        {
            if (seqN != NULL) *seqN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
            if (ackN != NULL) *ackN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        } 
        else 
        {
            return 1;
        }
    } 
    else 
    {
        return 1;
    }

    // Verify that the DUT  in FINWAIT1 state
    if(checkState(FINWAIT1))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

bool moveDUTToFINWAIT2(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN)
{
    // Move DUT to FIN-WAIT1 state   
    moveDUTToFINWAIT1(socketDUT, portDUT, portTESTER, maxCon, seqN, ackN);

    // Send ACK
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(*ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(*seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Verify that the DUT  in FINWAIT2 state
    if(checkState(FINWAIT2))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

//bool moveDUTToClosing(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN)
//{
//    // Move DUT to FIN-WAIT1 state   
//    moveDUTToFINWAIT1(socketDUT, portDUT, portTESTER, maxCon, seqN, ackN);
//
//    // Send FIN
//    TCP_Packet TCP_P = CreateTCP();
//    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
//    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
//    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
//    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(*ackN));
//    TCP_Compute_checksum(&TCP_P);
//    SendTCP(TCP_P);
//
//    // Receive ACK
//    Packet_filter f;
//    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
//    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
//    f.Srcport = TCPConfig.DUT_Port;
//    f.Dstport = TCPConfig.Tester_Port;
//
//    // Sniffing and verifying DUT response
//    TCP_Packet TCP_RP = ListenTCP(f);
//    if (TCP_RP.length != 0) 
//    {
//        printf("\n ACK packet received %s : %d \n", __FILE__, __LINE__);
//        int rACK  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
//        int rFIN  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagFIN);
//        if ((rACK == 1) && (rFIN == 0)) 
//        {
//            if (seqN != NULL) *seqN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
//            if (ackN != NULL) *ackN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
//        } 
//        else 
//        {
//            return 1;
//        }
//    } 
//    else 
//    {
//        printf("\n No packet received %s : %d \n", __FILE__, __LINE__);
//        return 1;
//
//    }
//
//    // Verify that the DUT  in CLOSING state
//    if(checkState(CLOSING))
//    {
//        return 0;
//    }
//    else
//    {
//        return 1;
//    }
//}

bool moveDUTToTimeWait(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN)
{
     // Move DUT to FIN-WAIT1 state   
    moveDUTToFINWAIT1(socketDUT, portDUT, portTESTER, maxCon, seqN, ackN);

    // Send FIN,ACK
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(*ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(*seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Receive ACK
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;

    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    {
        printf("\n ACK packet received %s : %d \n", __FILE__, __LINE__);
        int rACK  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1) 
        {
            if (seqN != NULL) *seqN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
            if (ackN != NULL) *ackN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        } 
        else 
        {
            return 1;
        }
    } 
    else 
    {
        printf("\n No packet received %s : %d \n", __FILE__, __LINE__);
        return 1;

    }

    // Verify that the DUT  in TIMEWAIT state
    if(checkState(TIMEWAIT))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

bool moveDUTToSYNSENT(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN) 
{
    ip4addr ipv4;
    ipv4.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipv4.Data);
    // Cause DUT to issue an active open call
    TP_TcpCreateAndBind(CreateAndBind_cb, TRUE, portDUT, ipv4);
    sleep(3);
    *socketDUT = socketId;
    stoiIP(TCPConfig.TESTER_IP, ipv4.Data);
    TP_TcpConnect(NULL, socketId, portTESTER, ipv4);

    // Receive SYN
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;

    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    {
      if (seqN != NULL) *seqN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
      if (ackN != NULL) *ackN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
    }

    // Verify that the DUT  in SYNSENT state
    if(checkState(SYNSENT))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

bool moveDUTToCloseWait(int* socketDUT, uint16 portDUT, uint16 maxCon, uint32* seqN, uint32* ackN) 
{
    //Move to established state
    moveServDUTToEstablished(socketDUT, portDUT, maxCon, seqN, ackN);

    //send FIN,ACK
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)portDUT);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)*seqN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)*ackN);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //Receive ACK
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = portDUT;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    {
        int rACK  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1) 
        {
            if (seqN != NULL) *seqN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
            if (ackN != NULL) *ackN = (uint32)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        } 
        else 
        {
            return 1;
        }
    } 
    else 
    {
        return 1;

    }
    
    // Verify that the DUT  in CLOSEWAIT state
    if(checkState(CLOSEWAIT))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

bool moveDUTToLASTACK(int* socketDUT, uint16 portDUT, uint16 maxCon, uint32* seqN, uint32* ackN)
{
    moveDUTToCloseWait(socketDUT, portDUT, maxCon, seqN, ackN);
    sleep(3);
    // close socket
    TP_TcpCloseSocket(NULL, (uint16_t)*socketDUT, TRUE);

    // Verify that the DUT  in LASTACK state
    if(checkState(LASTACK))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

bool moveDUTToClosing(int* socketDUT, uint16 portDUT, uint16 portTESTER, uint16 maxCon, uint32* seqN, uint32* ackN)
{
    // Move DUT to FIN-WAIT1 state   
    moveClientDUTToEstablished(socketDUT, portDUT, portTESTER, maxCon, seqN, ackN);

    // close socket
    TP_TcpCloseSocket(NULL, (uint16_t)*socketDUT, FALSE);
    sleep(3);

    //send FIN,ACK
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)portDUT);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(*ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(*seqN));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Verify that the DUT  in CLOSING state
    if(checkState(CLOSING))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

int StartTest()
{
    ip4addr ipv4;
    ipv4.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipv4.Data);
    //TP_OpenControlChannel(ipv4, 56000);
    TP_GenaralStartTest(NULL);
}

int EndTest(int SocketID)
{
    if(SocketID!=-1)
    {
        TP_TcpCloseSocket(NULL, (uint16_t)SocketID, TRUE);
    }
    TP_GenaralEndTest(NULL,0,(text){0});
}
