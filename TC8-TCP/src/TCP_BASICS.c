#include "AbstractionAPI.h"
#include "TCPcommon.h"
#include "TCP_BASICS.h"

#include"TestabilityProtocol_api.h"

int sock = 0;

void CreateAndBind_callback(TP_ResultID_t b, uint16 s)
{
    sock = s;

}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_01
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_01() {
    // Start Test 
    StartTest();

    // 1. TESTER: Cause DUT to move on to LISTEN state at a <wnp>
    int socketId;
    moveDUTToListen(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon);

    // 2. TESTER: Send a SYN to DUT at <wnp>
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 3. DUT: Send SYN,ACK
    // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        // Verify that DUT response is SYN,ACK
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSYN == 1) && (rACK == 1))
        {
            printf("\nTest passed\n");
            EndTest(socketId);
              return 0;
        }
        else
        {
            // Unexpected response
            printf("\nTest failed --> DUT response is not SYN,ACK\n");
            EndTest(socketId);
              return 1;
        }
    }
    else
    {
        // No response sent
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
              return 1;
    }
	
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_02
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_02() {
    // Start Test
    StartTest();

    // Move DUT to Listen state
    int socketId;
    moveDUTToListen(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon);

    // 1. TESTER: Send a SYN to DUT 
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 2. DUT: Send a SYN,ACK(this will take DUT to the state SYN-RCVD)
    // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        // Verify that DUT response is SYN,ACK
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSYN == 1) && (rACK == 1))
        {
            // TODO : ADD LOGS
        }
        else
        {
            // Unexpected response
            printf("\nTest failed --> DUT response is not SYN,ACK\n");
            EndTest(socketId);
            return 1;
        }
    }
    else
    {
        // No response sent
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
              return 1;
    }

    // 3. TESTER: Send an ACK
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 4. TESTER: Verify that the DUT moves on to ESTABLISHED state
    if(checkState(ESTABLISHED))
    {
        printf("\nTest passed\n");
        EndTest(socketId);
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
        EndTest(socketId);
        return 1;
    }
	  
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_03
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 3 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_03() {
    // Start Test
    StartTest();
    
    // 1. TESTER: Cause the DUT to move on to ESTABLISHED state at a <wnp> 
    int socketId;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, (uint32*)NULL, (uint32*)NULL);

    // 2. TESTER: Send a FIN ,ACK
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 3. DUT: Send ACK
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if(rACK == 1)
        {
            printf("test passed");
            EndTest(socketId);
            return 0;
        }
        else
        {
            // Unexpected response
            printf ("\nTest failed --> DUT response is not ACK\n");
            EndTest(socketId);
            return 1;
        }
    }
    else
    {
        // No response sent
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_04_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 4 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_04_IT1() {

    // 1. TESTER: Send a TCP segment with a flag set 
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 2. DUT: Send a RST control message with zero SEQ number
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) { 
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        int rSEQ = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if ((rRST == 1) && (rSEQ == 0)) {
            printf("Test passed\n");
            EndTest(-1);
            return 0;
        } else {
            printf ("Test failed --> RST field is not set or sequence number != 0\n");
            EndTest(-1);
            return 1;
        }
    } else {
        printf("Test failed --> DUT not responding\n");
        EndTest(-1);
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_04_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 4 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_04_IT2() {

    // 1. TESTER: Send a TCP segment with a flag set
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 2. DUT: Send a RST control message with zero SEQ number
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) { 
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        int rSEQ = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if ((rRST == 1) && (rSEQ == 0))
        {
            printf("Test passed\n");
            EndTest(-1);
            return 0;
        }
        else
        {
            printf ("Test failed --> RST field is not set or sequence number # 0\n");
            EndTest(-1);
            return 1;
        }
    }
    else
    {
        printf("Test failed --> DUT not responding\n");
        EndTest(-1);
        return 1;
        }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_04_IT3
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 4 iteration 3 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_04_IT3() {

    // 1. TESTER: Send a TCP segment with a flag set
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 2. DUT: Send a RST control message with zero SEQ number
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) { 
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        int rSEQ = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if ((rRST == 1) && (rSEQ == 0)) {
            printf("Test passed\n");
            EndTest( -1);
            return 0;
        } else {
            printf ("Test failed --> RST field is not set or sequence number # 0\n");
            EndTest( -1);
            return 1;
        }
    } else {
        printf("Test failed --> DUT not responding\n");
        EndTest( -1);
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_05_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 5 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_05_IT1() {

    // 1. TESTER: Send a segment with a flag set
    TCP_Packet TCP_P = CreateTCP();
    int ackNum = 222;
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)ackNum);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 2. DUT: Send a RST segment with SEQ number same as the ACK  number of the incoming segment
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) { 
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        int seqNum = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if ((rRST == 1) && (seqNum == ackNum)) {
            printf("Test passed\n");
            EndTest( -1);
            return 0;
        } else {
            printf ("Test failed --> RST field is not set or #SEQ != #ACK\n");
            EndTest( -1);
            return 1;
        }
    } else {
        printf("Test failed --> DUT not responding\n");
        EndTest( -1);
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_05_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 5 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_05_IT2() {

    // 1. TESTER: Send a segment with a flag set
    TCP_Packet TCP_P = CreateTCP();
    int ackNum = 222;
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)ackNum);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 2. DUT: Send a RST segment with SEQ number same as the ACK  number of the incoming segment
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) { 
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        int seqNum = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if ((rRST == 1) && (seqNum == ackNum)) {
            printf("Test passed\n");
            EndTest(-1);
            return 0;
        } else {
            printf ("Test failed --> RST field is not set or #SEQ != #ACK\n");
            EndTest( -1);
            return 1;
        }
    } else {
        printf("Test failed --> DUT not responding\n");
        EndTest( -1);;
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_06
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 6 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_06() {

    int socketId;
    ip4addr ipv4;
    ipv4.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipv4.Data);
    // Start Test
    StartTest();

    // 1. TESTER: Cause the application on the DUT-side to issue an   active OPEN call
    TP_TcpCreateAndBind(CreateAndBind_callback, TRUE, TCPConfig.DUT_Port, ipv4);
    sleep(3);
    socketId = sock;
    stoiIP(TCPConfig.TESTER_IP, ipv4.Data);
    TP_TcpConnect(NULL, socketId, TCPConfig.Tester_Port, ipv4);

    // 2. DUT: Send a SYN
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        // Verify that DUT sends a SYN
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if (rSYN == 1) {
            printf("\nTest passed\n");
            EndTest(socketId);
            return 0;
        } else {
            // Unexpected response
            printf("\nTest failed --> DUT response is not SYN\n");
            EndTest(socketId);
            return 1;
        }
    } else {
        // DUT nor reponding
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }
	
     
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_07
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 7 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_07() {
    // Start Test
    StartTest();

    // 1. TESTER: Cause the DUT to move on to SYN-SENT state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToSYNSENT(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 2. TESTER: Send a SYN,ACK
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 3. DUT: Send ACK
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        // Verify that DUT sends a ACK
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1) {
            // Test OK
        } else {
            // Unexpected response
            printf("\nTest failed --> DUT response is not ACK\n");
            EndTest(socketId);
            return 1;
        }
    } else {
        // No response sent
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }

    // 4. TESTER: Verify that the DUT moves to ESTABLISHED state
    if(checkState(ESTABLISHED))
    {
        printf("\nTest passed\n");
        EndTest(socketId);
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
        EndTest(socketId);
              return 1;
    }

}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_08_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs iteration 1 of TCP BASICS 8 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_08_IT1() {
    // Start Test
    StartTest();

    // 1. TESTER: Cause the DUT to move on to <wst> state 
    int socketId;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, (uint32*)NULL, (uint32*)NULL);

    // 2. TESTER: Cause the application on the DUT-side to issue a CLOSE call
    TP_TcpCloseSocket(NULL, (uint16)socketId, TRUE);

    // 3. DUT: Send a FIN
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        // Verify that DUT sends a FIN
        int rFIN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagFIN);
        if (rFIN == 1) {
            printf("\nTest passed\n");
            EndTest(socketId);
            return 0;
        } else {
            // Unexpected response
            printf("\nTest failed --> DUT response is not FIN\n");
            EndTest(socketId);
            return 1;
        }
    } else {
        // DUT not responding
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }

}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_08_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs iteration 2 of TCP BASICS 8 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_08_IT2() {
    // Start Test
    StartTest();

    // 1. TESTER: Cause the DUT to move on to <wst> state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 2. TESTER: Cause the application on the DUT-side to issue a CLOSE call
    TP_TcpCloseSocket(NULL, (uint16)socketId, TRUE);

    // 3. DUT: Send a FIN
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        // Verify that DUT sends a FIN
        int rFIN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagFIN);
        if (rFIN == 1) {
            EndTest(socketId);
            return 0;
        } else {
            // Unexpected response
            printf("\nTest failed --> DUT response is not FIN\n");
            EndTest(socketId);
            return 1;
        }
    } else {
        // DUT not responding
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }
   
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_09
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 9 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_09() {
    // Start Test
    StartTest();

    // 1. TESTER: Cause the DUT to move on to LAST-ACK state
    int socketId;
    uint32 seqN, ackN;
    // 2. DUT: Send FIN when moving into LAST-ACK state
    moveDUTToLASTACK(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 3. TESTER: Send ACK for the FIN just received from the DUT
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 4. TESTER: Send a segment without the RST flag set
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 5. DUT: Send a segment with the RST flag set (this will verify the the DUT has moved on to the CLOSED state)
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        // Verify that DUT sends a RST
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (rRST == 1) {
            printf("\nTest passed\n");
            EndTest(socketId);
            return 0;
        } else {
            // Unexpected response
            printf("\nTest failed --> DUT response is not RST\n");
            EndTest(socketId);
            return 1;
        }
    } else {
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }

}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_10_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs iteration 1 of TCP BASICS 10 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_10_IT1()
{
    // Start Test
    StartTest();

    // 1. TESTER: Cause the DUT to move on to FINWAIT-1 state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT1(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 2. TESTER: Send a FIN
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 3. DUT: Send ACK
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        // Verify that DUT sends an ACK
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1)
        {
            EndTest(socketId);
            printf("\nTest passed\n");
            return 0;
        }
        else
        {
            // Unexpected response
            printf("\nTest failed --> DUT response is not ACK\n");
            EndTest(socketId);
            return 1;
        }
    }
    else
    {
        // DUT not responding
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }

}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_10_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs iteration 2 of TCP BASICS 10 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_10_IT2()
{
    // Start Test
    StartTest();

    // 1. TESTER: Cause the DUT to move on to FINWAIT-2 state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 2. TESTER: Send a FIN
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 3. DUT: Send ACK
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        // Verify that DUT sends an ACK
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1)
        {
            printf("\nTest passed\n");
            EndTest(socketId);
            return 0;
        }
        else
        {
            // Unexpected response
            printf("\nTest failed --> DUT response is not ACK\n");
            EndTest(socketId);
            return 1;
        }
    }
    else
    {
        // DUT not responding
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }

     
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_11
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 11 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_11()
{
    // Start Test
    StartTest();

    // 1. TESTER: Cause the DUT to move on to FINWAIT-2 state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 2. TESTER: Send a FIN
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 3. DUT: Send ACK
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        // Verify that DUT sends an ACK
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1) {
            //Test ok
        } 
        else 
        {
            printf("\nTest failed --> DUT response is not ACK\n");
            EndTest(socketId);
            return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }

    // 4. TESTER: Send a FIN after 2*MSL + 20%
    sleep(2.2*TCPConfig.msl);
    
    // 5. DUT: Send a RST segment(this will indicate DUT is in CLOSED state)
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //Receive DUT responses
    TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        // Verify that DUT sends an RST
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (rRST == 1) 
        {
            printf("\nTest passed\n");
            EndTest(socketId);
            return 0;
        } 
        else 
        {
            // Unexpected response
            printf("\nTest failed --> DUT did not move to closed state\n");
            EndTest(socketId);
            return 1;
        }
    } 
    else 
    {
        // DUT not responding
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
              return 1;
    }
   
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_12
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 12 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_12()
{
    // Start Test
    StartTest();

    // 1. TESTER: Cause the DUT to move on to CLOSING state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToClosing(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 2. TESTER: Send ACK for the FIN just received from the DUT
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // DUT moves to TIME-WAIT 

    // 3. TESTER: Send a FIN after 2*MSL + 20%
    sleep(2.2*TCPConfig.msl);
    // Send FIN segment
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 4. DUT: Send a RST segment(this will indicate DUT is in CLOSED state) 
    //Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        // Verify that DUT sends an RST
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (rRST == 1) 
        {
            printf("\nTest passed\n");
            EndTest(socketId);
            return 0;
        } 
        else 
        {
            // Unexpected response
            printf("\nTest failed --> DUT did not move to closed state\n");
            EndTest(socketId);
            return 1;
        }
    } 
    else 
    {
        // DUT not responding
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }

     
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_13
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 13 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_13()
{
    // Start Test
    StartTest();

    // 1. TESTER: Cause the DUT to move on to FINWAIT-2 state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 2. TESTER: Send a FIN
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 3. DUT: Send ACK (DUT moves to TIME-WAIT state)
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        // Verify that DUT sends an ACK
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1) 
        {
            //Test ok
        } 
        else 
        {
            // Unexpected response
            printf("\nTest failed --> DUT did not move to closed state\n");
            EndTest(socketId);
            return 1;
        }
    } 
    else 
    {
        // DUT not responding
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }

    // 4. TESTER: Send a FIN within 2*MSL time
    int percentage = 1.6;
    sleep(percentage*TCPConfig.msl);

    // 5. DUT: Send ACK
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //Receive DUT responses
    TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        // Verify that DUT sends an ACK
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1) 
        {
            printf("\nTest passed\n");
            EndTest(socketId);
            return 0;
        } 
        else 
        {
            // Unexpected response
            printf("\nTest failed --> DUT did not move to closed state\n");
            EndTest(socketId);
            return 1;
        }
    } 
    else 
    {
        // DUT not responding
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }

}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_14
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 14 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_14()
{
    // Start Test
    StartTest();

    // 1. TESTER: Cause the DUT to move on to CLOSING state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToClosing(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 2. TESTER: Send ACK for the FIN just received from the DUT (DUT moves to TIME-WAIT state)
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // DUT moves to TIME-WAIT 

    // 3. TESTER: Send a FIN within 2*MSL time
    int percentage = 1.6;
    sleep(percentage*TCPConfig.msl);

    // Send FIN segment
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)ackN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 4. DUT: Send ACK 
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        // Verify that DUT sends an ACK
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1) 
        {
            printf("\nTest passed\n");
            EndTest(socketId);
            return 0;
        } 
        else 
        {
            // Unexpected response
            printf("\nTest failed --> DUT did not move to closed state\n");
            EndTest(socketId);
            return 1;
        }
    } 
    else 
    {
        // DUT not responding
        printf("\nTest failed --> DUT not responding\n");
        EndTest(socketId);
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_BASICS_17
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP BASICS 17 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_BASICS_17()
{
    return 2;
}