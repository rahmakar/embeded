#include "AbstractionAPI.h"
#include "TCPcommon.h"
#include "TCP_UNACCEPTABLE.h"

#include"TestabilityProtocol_api.h"
#include"TestabilityProtocol_Intern.h"

extern uint8 state ;

int TCP_UNACCEPTABLE_01() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    uint32 rSEQNumber;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port, 5, &rSEQNumber, (uint32*)NULL);

    // Send RST segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x06);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(rSEQNumber + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    {
        // Check if DUT is in LISTEN state
        if(checkState(LISTEN))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else
        {
            printf("\nTest failed --> DUT is not in LISTEN state\n");
            return 1;
        }
    }
    else
    {
        printf("\nTest failed --> DUT responded to RST segment\n");
        return 1;
    }
	
    TP_GenaralEndTest(NULL,0,(text){0});
    TP_CloseControlChannel();
}

int TCP_UNACCEPTABLE_02() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    uint32 rSEQNumber;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port, 5, &rSEQNumber, (uint32*)NULL);

    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0xFF);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(rSEQNumber + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0) {
        // Verify that the DUT in SYN-RCVD state
        if(checkState(SYNRCVD))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else
        {
            printf("\nTest failed --> DUT is not in SYN-RCVD state\n");
            return 1;
        }
    }
    else
    {
        printf("\nTest failed --> DUT responded to RST segment\n");
        return 1;
    }

    TP_GenaralEndTest(NULL,0,(text){0});
    TP_CloseControlChannel();
}

int TCP_UNACCEPTABLE_03() 
{
    //open controle connection
    //TP_OpenControlChannel();
    //TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port, 5, &seqN, (uint32*)NULL);

    // Send segment with unacceptable ACK number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)seqN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(0xFFF));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (rRST == 1)
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not RST\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    TP_GenaralEndTest(NULL,0,(text){0});
    TP_CloseControlChannel();
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_04_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_04 test iteration 1.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* - CASE 1: Data segment with out of window SEQ number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_04_IT1()
{

    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to ESTABLISHED state
    int socketId;
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment with out of window SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(seqN + 50));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is an ACK segment
    if (TCP_RP.length != 0) 
    {
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if (rACK == 1)
        {
            int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
            int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
            if ((rSEQN == ackN+1) && (rACKN == seqN))
            {
                //TEST OK
            }
        }
        else{
            printf("\nTest failed --> DUT response is not ACK\n");
            return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the ESTABLISHED state 
    if(checkState(ESTABLISHED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_04_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_04 test iteration 2.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -  CASE 2: Data segment with an unacceptable ACK number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_04_IT2()
{

    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to ESTABLISHED state
    int socketId;
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment satisfying one of the following cases
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN+1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)seqN);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        if ((rSEQN == ackN+1) && (rACKN == seqN))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not RST\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the ESTABLISHED state 
    if(checkState(ESTABLISHED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}


int TCP_UNACCEPTABLE_05_IT1() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    moveDUTToListen(&socketId, TCPConfig.DUT_Port, 5);

    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x05);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0x20);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (rRST == 1) {
            // Verify that is in LISTEN state
            if(checkState(LISTEN))
            {
                printf("\nTest passed\n");
                TP_GenaralEndTest(NULL,0,(text){0});
                return 0;
            }
            else
            {
                printf("\nTest failed --> DUT is not in LISTEN state\n");
                TP_GenaralEndTest(NULL,0,(text){0});
                return 1;
            }
        }
    }
    else
    {
        printf("\nTest failed --> DUT not responding\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

int TCP_UNACCEPTABLE_05_IT2() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to LISTEN state
    int socketId;
    moveDUTToListen(&socketId, TCPConfig.DUT_Port, 5);

    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x05);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0x20);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (rRST == 1) {
            // Verify that DUT is in LISTEN state
            if(checkState(LISTEN))
            {
                printf("\nTest passed\n");
                TP_GenaralEndTest(NULL,0,(text){0});
                return 0;
            }
            else
            {
                printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
                TP_GenaralEndTest(NULL,0,(text){0});
                return 1;
            }
        }
    }
    else
    {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
	
    TP_GenaralEndTest(NULL,0,(text){0});
    TP_CloseControlChannel();
}

int TCP_UNACCEPTABLE_06() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, (uint32*)NULL, (uint32*)NULL);

    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x50);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1) {
            int rACKNumber = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
            if(rACKNumber == 6) {
                printf("\nTest passed\n");
                return 0;
            } else {
                printf("\nTest failed --> DUT responded with an unexpected ACK number\n");
                return 1;
            }
        }
    } else {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
    //return 0;
	
    TP_GenaralEndTest(NULL,0,(text){0});
    TP_CloseControlChannel();
}

int TCP_UNACCEPTABLE_07() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    moveDUTToListen(&socketId, TCPConfig.DUT_Port, 5);

    // Send SYN,ACK segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    //Verify that DUT response is a RST
    if (TCP_RP.length != 0) {
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (rRST == 1) {
            printf("test passed");
            return 0;
        }
    } else {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
    //return 0;
	
    TP_GenaralEndTest(NULL,0,(text){0});
    TP_CloseControlChannel();
}

int TCP_UNACCEPTABLE_08_IT1()
{
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    moveDUTToSYNSENT(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, 5, (uint32*)NULL, (uint32*)NULL);

    // Send SYN,ACK segment
    int sACK = 0xFF;
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)sACK);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        int rSeqNumber = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if ((rRST == 1) && (rSeqNumber == sACK)) 
        {
            printf("\ntest passed\n");
            return 0;
        }
        else
        {
            printf("\nTest failed --> DUT response is unexpected\n");
            return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
    //return 0;
	
    TP_GenaralEndTest(NULL,0,(text){0});
    TP_CloseControlChannel();
}

int TCP_UNACCEPTABLE_08_IT2()
{
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    moveDUTToSYNSENT(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, 5, (uint32*)NULL, (uint32*)NULL);

    // Send ACK segment
    int sACK = 0xFF;
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)sACK);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        int rSeqNumber = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if ((rRST == 1) && (rSeqNumber == sACK)) 
        {
            printf("\ntest passed\n");
            return 0;
        }
        else
        {
            printf("\nTest failed --> DUT response is unexpected\n");
            return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }
    //return 0;
	
    TP_GenaralEndTest(NULL,0,(text){0});
    TP_CloseControlChannel();
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_09_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_09 test iteration 1.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* - CASE 1: Data segment with out of window SEQ number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_09_IT1()
{
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to FINWAIT-1 state
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 30000;
    moveDUTToFINWAIT1(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment with out of window SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSEQN == seqN + 1) && (rACKN == ackN) && (rACK == 1))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else
        {
        printf("\nTest failed --> DUT response is not ACK\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the FINWAIT-1 state 
    if(checkState(FINWAIT1))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT1 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_09_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_09 test iteration 2.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -  CASE 2: Data segment with an unacceptable ACK number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_09_IT2()
{
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to FINWAIT-1 state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT1(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment satisfying one of the following cases
    //2. TESTER: Send a data segment with out of window SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN - 200));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSEQN == seqN + 1) && (rACKN == ackN) && (rACK == 1))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not ACK\n");
        return 1;
        }
    }
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the FINWAIT-1 state 
    if(checkState(FINWAIT1))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT1 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_10_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_10 test iteration 1.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* - CASE 1: Data segment with out of window SEQ number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_10_IT1()
{
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to FINWAIT-2 state
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 30000;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment with out of window SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSEQN == seqN + 1) && (rACKN == ackN) && (rACK == 1))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else
        {
        printf("\nTest failed --> DUT response is not ACK\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the FINWAIT-2 state 
    if(checkState(FINWAIT2))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT2 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_10_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_10 test iteration 2.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -  CASE 2: Data segment with an unacceptable ACK number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_10_IT2()
{
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to FINWAIT-2 state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment satisfying one of the following cases
    //2. TESTER: Send a data segment with out of window SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(0xFFFFFFF1));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSEQN == seqN + 1) && (rACKN == ackN) && (rACK == 1))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not ACK\n");
        return 1;
        }
    }
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the FINWAIT-2 state 
    if(checkState(FINWAIT2))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT2 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_11_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_11 test iteration 1.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* - CASE 1: Data segment with out of window SEQ number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_11_IT1()
{
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to Closing state
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 30000;
    moveDUTToClosing(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment with out of window SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSEQN == seqN + 1) && (rACKN == ackN + 1) && (rACK == 1))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else
        {
        printf("\nTest failed --> DUT response is not ACK\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the CLOSING state 
    if(checkState(CLOSING))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in Closing state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_11_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_11 test iteration 2.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -  CASE 2: Data segment with an unacceptable ACK number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_11_IT2()
{
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to FINWAIT-2 state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToClosing(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment satisfying one of the following cases
    //2. TESTER: Send a data segment with out of window SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(0xFFFFFFF1));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSEQN == seqN + 1) && (rACKN == ackN + 1) && (rACK == 1))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not ACK\n");
        return 1;
        }
    }
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the CLOSING state 
    if(checkState(CLOSING))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in CLOSING state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_12_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_12 test iteration 1.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* - CASE 1: Data segment with out of window SEQ number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_12_IT1()
{

    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to CLOSING state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToLASTACK(&socketId, TCPConfig.DUT_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment satisfying one of the following cases
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(seqN-5));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        if ((rSEQN == ackN+1) && (rACKN == seqN))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not RST\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the LAST-ACK  state 
    if(checkState(LASTACK))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in LAST-ACK state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_12_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_12 test iteration 2.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -  CASE 2: Data segment with an unacceptable ACK number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_12_IT2()
{

    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to CLOSING state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToLASTACK(&socketId, TCPConfig.DUT_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment satisfying one of the following cases
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN+1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)seqN);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        if ((rSEQN == ackN+1) && (rACKN == seqN))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not RST\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the LAST-ACK  state 
    if(checkState(LASTACK))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in LAST-ACK state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_13_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_13 test iteration 1.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* - CASE 1: Data segment with out of window SEQ number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_13_IT1()
{

    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to CLOSING state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToTimeWait(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, 5, &seqN, &ackN);
    //2. TESTER: Send a data segment satisfying one of the following cases
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(seqN-5));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        if ((rSEQN == ackN+1) && (rACKN == seqN))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not RST\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the TIME-WAIT state 
    if(checkState(TIMEWAIT))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in TIME-WAIT state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_13_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_13 test iteration 2.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -  CASE 2: Data segment with an unacceptable ACK number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_13_IT2()
{

    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to CLOSING state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToTimeWait(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment satisfying one of the following cases
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN+1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)seqN);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        if ((rSEQN == ackN+1) && (rACKN == seqN))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not RST\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the TIME-WAIT state 
    if(checkState(TIMEWAIT))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in TIME-WAIT state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_14_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_14 test iteration 1.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* - CASE 1: Data segment with out of window SEQ number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_14_IT1()
{

    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to CLOSING state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketId, TCPConfig.DUT_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment satisfying one of the following cases
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(seqN-5));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        if ((rSEQN == ackN+1) && (rACKN == seqN))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not RST\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the CLOSING  state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if( state == CLOSING)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_UNACCEPTABLE_14_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_UNACCEPTABLE_14 test iteration 2.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -  CASE 2: Data segment with an unacceptable ACK number
*
* @warning
* -
*
***************************************************************************************************/
int TCP_UNACCEPTABLE_14_IT2()
{

    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to CLOSING state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketId, TCPConfig.DUT_Port, 5, &seqN, &ackN);

    //2. TESTER: Send a data segment satisfying one of the following cases
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN+1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)seqN);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with current send SEQ number and ACK number indicating next SEQ number expected
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    // Check that DUT response is a RST segment
    if (TCP_RP.length != 0) 
    {
        int rSEQN = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rACKN = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        if ((rSEQN == ackN+1) && (rACKN == seqN))
        {
            printf("\nTest passed\n");
            return 0;
        }
        else{
        printf("\nTest failed --> DUT response is not RST\n");
        return 1;
        }
    } 
    else 
    {
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in the CLOSING state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSING)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
}