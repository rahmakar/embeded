#include "AbstractionAPI.h"
#include "TCPcommon.h"
#include "TCP_CHECKSUM.h"


#include"TestabilityProtocol_api.h"
#include"TestabilityProtocol_Intern.h"

int TCP_CHECKSUM_01() {
    // Start Test
    TP_GenaralStartTest(NULL);

    // 1. TESTER: Cause the DUT to move on to ESTABLISHED state at a <wnp>
    int socketId;
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 2. TESTER: Send a data segment with correct checksum
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(seqN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // 3. DUT: Send ACK 
    // Receive DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        // verify that DUT sends an ACK segment
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1) {
            printf("\nTest passed\n");
            TP_GenaralEndTest(NULL,0,(text){0});
            return 0;
        } else {
            // Unexpected response
            printf("\nTest failed --> DUT response is not ACK\n");
            TP_GenaralEndTest(NULL,0,(text){0});
            return 1;
        }
    } else {
        // DUT not responding
        printf("\nTest failed --> DUT is not responding\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
	
}

int TCP_CHECKSUM_02() {
    // Start Test
    TP_GenaralStartTest(NULL);

    // 1. TESTER: Cause DUT to move on to ESTABLISHED state at a <wnp>
    int socketId;
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // 2. TESTER: Send a data segment with incorrect checksum
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(seqN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    SendTCP(TCP_P);

    // 3. DUT: Do not send ACK
    // Recieve DUT responses
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0) {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    } else {
        // Unexpected response
        printf("\nTest failed --> DUT responded while it shouldn't\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CHECKSUM_03
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP CHECKSUM 03 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CHECKSUM_03()
{
    // Start Test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to ESTABLISHED state
    int socketId;
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Cause the application on the DUT-side to issue a SEND request for a data segment
    vint8 data;
    data.Data = (uint8*)"data";
    data.dataLength = 4;
    TP_TcpSendData(NULL, socketId, 9,  0, data);
    
    
    //3. DUT: Send the data segment
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) {
        //4. TESTER: Verify that correct checksum is present in the incoming segment 
        if (TCP_Verify_Correct_checksum(TCP_RP) == 0) 
        {
            printf("\nTest passed\n");
            return 0;
        } 
        else 
        {
            // Unexpected response
            printf("\nTest failed --> checksum incorrect\n");
            return 1;
        }
    } else {
        // DUT not responding
        printf("\nTest failed --> DUT is not responding\n");
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CHECKSUM_04
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP CHECKSUM 04 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CHECKSUM_04()
{
    return 2;
}