#include "TCP_OUT_OF_ORDER.h"

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_OUT_OF_ORDER_01
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_OUT_OF_ORDER_01 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_OUT_OF_ORDER_01()
{
    return 2;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_OUT_OF_ORDER_02
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_OUT_OF_ORDER_02 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_OUT_OF_ORDER_02()
{
    return 2;
}
/***************************************************************************************************
*
*   FUNCTION NAME: TCP_OUT_OF_ORDER_03
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_OUT_OF_ORDER_03 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_OUT_OF_ORDER_03()
{
    return 2;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_OUT_OF_ORDER_05
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_OUT_OF_ORDER_05 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_OUT_OF_ORDER_05()
{
    return 2;
}
