#include "TCP_PROBING_WINDOWS.h"

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_PROBING_WINDOWS_02
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP PROBING WINDOWS 02 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_PROBING_WINDOWS_02()
{
    return 2;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_PROBING_WINDOWS_03
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP PROBING WINDOWS 02 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_PROBING_WINDOWS_03()
{
    return 2;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_PROBING_WINDOWS_04
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP PROBING WINDOWS 02 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_PROBING_WINDOWS_04()
{
    return 2;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_PROBING_WINDOWS_05
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP PROBING WINDOWS 02 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_PROBING_WINDOWS_05()
{
    return 2;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_PROBING_WINDOWS_06
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP PROBING WINDOWS 02 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_PROBING_WINDOWS_06()
{
    return 2;
}



