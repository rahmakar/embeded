#include "AbstractionAPI.h"
#include "TCPcommon.h"
#include "TCP_FLAGS_INVALID.h"


#include"TestabilityProtocol_api.h"
#include"TestabilityProtocol_Intern.h"

extern uint8 state ;

int TCP_FLAGS_INVALID_01() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    moveDUTToListen(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon);

    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        printf("\nTest failed --> DUT responded while it shoudn't\n");
        return 1;
    }

    // Verify that DUT is in LISTEN state
    if(checkState(LISTEN))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in LISTEN state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

int TCP_FLAGS_INVALID_02() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    moveDUTToListen(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon);

    // Send SYN,ACK segment
    TCP_Packet TCP_P = CreateTCP();
    int ACKNumber = 55;
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)ACKNumber);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    {
        int rSEQNumber = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        int rRST = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if((rSEQNumber == ACKNumber) && (rRST == 1))
        {
            //TEST OK
        }
        else
        {
            printf("\nTest failed --> DUT response not RST or with unexpected SEQ number\n");
            TP_GenaralEndTest(NULL,0,(text){0});
            return 1;
        }
    } else {
        printf("\nTest failed --> DUT not responding\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }

    // Verify that DUT is in LISTEN state
    if(checkState(LISTEN))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in LISTEN state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

int TCP_FLAGS_INVALID_03() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    moveDUTToSYNSENT(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, (uint32*)NULL, (uint32*)NULL);

    // Send SYN,RST segment
    TCP_Packet TCP_P = CreateTCP();
    int ACKNumber = 55;
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)ACKNumber);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0) 
    {
        //Test OK
    } 
    else 
    {
        printf("\nTest failed --> DUT responded to a RST segment\n");
        return 1;
    }
    //4. TESTER: Verify that the DUT remains in SYN-SENT state
    if(checkState(SYNSENT))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in SYN-SENT state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

int TCP_FLAGS_INVALID_04() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    moveDUTToSYNSENT(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, (uint32*)NULL, (uint32*)NULL);

    // Send a RST segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    // Verify that DUT ignored the RST segment
    if (TCP_RP.length == 0) 
    {
        //Test OK
    } 
    else 
    {
        printf("\nTest failed --> DUT responded to a RST segment\n");
        return 1;
    }
    // Verify that the DUT remains in SYN-SENT state
    if(checkState(SYNSENT))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in SYN-SENT state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

int TCP_FLAGS_INVALID_05_IT1() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToSYNSENT(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // Send segment with SYN,ACK and RST flags set
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Verify that the DUT move to CLOSED state
    if(checkState(CLOSED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in CLOSED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

int TCP_FLAGS_INVALID_05_IT2() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToSYNSENT(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // Send segment with ACK and RST flags set
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Verify that the DUT move to CLOSED state
    if(checkState(CLOSED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in CLOSED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

int TCP_FLAGS_INVALID_06_IT1() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToSYNSENT(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // Send segment with PSH and ACK flags set
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    // Verify that DUT does not respond
    if (TCP_RP.length != 0) 
    {
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSYN == 1) && (rACK == 1))
        {
            printf("\nTest failed --> DUT responded with a SYN,ACK\n");
            TP_GenaralEndTest(NULL,0,(text){0});
            return 1;  

        }
    } 
    else 
    {
        //Test OK
    }

    // Verify that the DUT remain in SYN-SENT state
    if(checkState(SYNSENT))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in SYN-SENT state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

int TCP_FLAGS_INVALID_06_IT2() {
    //open controle connection
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    // Move DUT to established state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToSYNSENT(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    // Send ACK segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

     // Receive DUT response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    // Verify that DUT does not respond
    if (TCP_RP.length != 0) 
    {
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rSYN == 1) && (rACK == 1))
        {
            printf("\nTest failed --> DUT responded with a SYN,ACK\n");
            TP_GenaralEndTest(NULL,0,(text){0});
            return 1;        }
    } 
    else 
    {
        //Test OK
    }

    // Verify that the DUT remain in SYN-SENT state
    if(checkState(SYNSENT))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in SYN-SENT state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_07_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_07 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_07_IT1()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to SYN-RCVD state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == ackN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in SYN-RCVD state 
    if(checkState(SYNRCVD))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in SYN-RCVD state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }   
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_07_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_07 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN,ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_07_IT2()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to SYN-RCVD state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == ackN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in SYN-RCVD state 
    if(checkState(SYNRCVD))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in SYN-RCVD state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_07_IT3
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_07 iteration 3 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_07_IT3()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to SYN-RCVD state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == ackN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in SYN-RCVD state 
    if(checkState(SYNRCVD))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in SYN-RCVD state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_07_IT4
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_07 iteration 4 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set FIN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_07_IT4()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to SYN-RCVD state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == ackN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in SYN-RCVD state 
    if(checkState(SYNRCVD))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in SYN-RCVD state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_07_IT5
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_07 iteration 5 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set Data segment 
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_07_IT5()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to SYN-RCVD state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == ackN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in SYN-RCVD state 
    if(checkState(SYNRCVD))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in SYN-RCVD state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_08_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_08 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_08_IT1()
{
   //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to ESTABLISHED state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == seqN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in ESTABLISHED state 
    if(checkState(ESTABLISHED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_08_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_08 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN,ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_08_IT2()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to ESTABLISHED state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == seqN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in ESTABLISHED state 
    if(checkState(ESTABLISHED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_08_IT3
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_08 iteration 3 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_08_IT3()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to ESTABLISHED state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == seqN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in ESTABLISHED state 
    if(checkState(ESTABLISHED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_08_IT4
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_08 iteration 4 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set FIN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_08_IT4()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to ESTABLISHED state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == seqN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            //TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else
    {
        //TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in ESTABLISHED state 
    if(checkState(ESTABLISHED))
    {
        printf("\nTest passed\n");
        //TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
        //TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_08_IT5
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_08 iteration 5 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set Data segment 
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_08_IT5()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to ESTABLISHED state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == seqN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in ESTABLISHED state 
    if(checkState(ESTABLISHED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_09_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_09 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_09_IT1()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to FINWAIT1 state 
    int socketId;
    uint32 seqN = 0, ackN = 0;
    int sSeq = 0x7530;
    moveDUTToFINWAIT1(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == seqN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in FINWAIT1 state 
    if(checkState(FINWAIT1))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT1 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_09_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_09 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN,ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_09_IT2()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to FINWAIT1 state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveDUTToFINWAIT1(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == seqN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in FINWAIT1 state 
    if(checkState(FINWAIT1))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT1 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_09_IT3
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_09 iteration 3 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_09_IT3()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause DUT to move on to FINWAIT1 state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveDUTToFINWAIT1(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == seqN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in FINWAIT1 state 
    if(checkState(FINWAIT1))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT1 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_09_IT4
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_09 iteration 4 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set FIN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_09_IT4()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause DUT to move on to FINWAIT1 state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveDUTToFINWAIT1(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == seqN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in FINWAIT1 state 
    if(checkState(FINWAIT1))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT1 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_09_IT5
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_09 iteration 5 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set Data segment 
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_09_IT5()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause DUT to move on to FINWAIT1 state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 0x7530;
    moveDUTToFINWAIT1(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(sSeq));
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        int rSYN = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagSYN);
        if ((rAckNum == seqN) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    }
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in FINWAIT1 state 
    if(checkState(FINWAIT1))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT1 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_10_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_10 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_10_IT1()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to FINWAIT-2 state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remainsin FINWAIT-2 state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == FINWAIT2)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_10_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_10 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN,ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_10_IT2()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to FINWAIT-2 state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remainsin FINWAIT-2 state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == FINWAIT2)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_10_IT3
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_10 iteration 3 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_10_IT3()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to FINWAIT-2 state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remainsin FINWAIT-2 state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == FINWAIT2)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_10_IT4
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_10 iteration 4 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set FIN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_10_IT4()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to FINWAIT-2 state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remainsin FINWAIT-2 state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == FINWAIT2)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_10_IT5
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_10 iteration 5 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set Data segment 
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_10_IT5()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to FINWAIT-2 state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remainsin FINWAIT-2 state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == FINWAIT2)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_11_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_11 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_11_IT1()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to CLOSE-WAIT state 
    int socketId;
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in CLOSE-WAIT state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSEWAIT)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_11_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_11 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN,ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_11_IT2()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to CLOSE-WAIT state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in CLOSE-WAIT state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSEWAIT)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_11_IT3
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_11 iteration 3 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_11_IT3()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to CLOSE-WAIT state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in CLOSE-WAIT state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSEWAIT)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_11_IT4
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_11 iteration 4 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set FIN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_11_IT4()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to CLOSE-WAIT state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in CLOSE-WAIT state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSEWAIT)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_11_IT5
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_11 iteration 5 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set Data segment 
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_11_IT5()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to CLOSE-WAIT state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in CLOSE-WAIT state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSEWAIT)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_09_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_12 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_12_IT1()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on  to CLOSING state 
    int socketId;
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in CLOSING state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSING)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_12_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_12 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN,ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_12_IT2()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on  to CLOSING state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToClosing(&socketId, TCPConfig.DUT_Port , TCPConfig.Tester_Port ,  TCPConfig.Maxcon, &seqN, &ackN);
    


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in CLOSING state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSING)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_12_IT3
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_12 iteration 3 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_12_IT3()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on  to CLOSING state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToClosing(&socketId, TCPConfig.DUT_Port , TCPConfig.Tester_Port ,  TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in CLOSING state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSING)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_12_IT4
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_12 iteration 4 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set FIN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_12_IT4()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on  to CLOSING state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToClosing(&socketId, TCPConfig.DUT_Port , TCPConfig.Tester_Port ,  TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in CLOSING state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSING)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_12_IT5
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_12 iteration 5 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set Data segment 
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_12_IT5()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on  to CLOSING state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToClosing(&socketId, TCPConfig.DUT_Port , TCPConfig.Tester_Port ,  TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in CLOSING state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSING)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_13_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_13 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_13_IT1()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to LAST-ACK state 
    int socketId;
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    
    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok   
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remainsin LAST-ACK state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == LASTACK)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_13_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_13 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = SYN,ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_13_IT2()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to LAST-ACK state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToLASTACK(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remainsin LAST-ACK state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == LASTACK)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_13_IT3
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_13 iteration 3 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_13_IT3()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to LAST-ACK state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToLASTACK(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remainsin LAST-ACK state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == LASTACK)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_13_IT4
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_13 iteration 4 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set FIN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_13_IT4()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to LAST-ACK state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToLASTACK(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(0));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remainsin LAST-ACK state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == LASTACK)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_13_IT5
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_13 iteration 5 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set Data segment 
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_13_IT5()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move  on to LAST-ACK state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToLASTACK(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);


    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == seqN+1) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify that the DUT remainsin LAST-ACK state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == LASTACK)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
    
}




/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_14_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_14 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = FIN
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_14_IT1()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to TIME-WAIT state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToTimeWait(&socketId, TCPConfig.DUT_Port , TCPConfig.Tester_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 30000));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == ackN) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify DUT remains in TIME-WAIT state for other cases 
    if(checkState(TIMEWAIT))
    {
        printf("\nTest passed\n");
        EndTest(socketId);
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in TIMEWAIT state\n");
        EndTest(socketId);
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_14_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_14 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: flag set = DATA Segment
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_14_IT2()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to TIME-WAIT state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToTimeWait(&socketId, TCPConfig.DUT_Port , TCPConfig.Tester_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a segment with a flag set, RST=0 and with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagPSH, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 30000));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send an ACK with ACK number indicating the correct expected next SEQ number
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    TCP_Packet TCP_RP = ListenTCP(f);

    if (TCP_RP.length != 0) 
    {
        int rAckNum = (int)GetTCPField(&TCP_RP, TCP, TCP_AckNumber);
        int rACK = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if ((rAckNum == ackN) && (rACK == 1))
        {
            // result ok        
        }
        else 
        {
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> response not ok\n");
            return 1;  
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT not responding\n");
        return 1;
    }


    //4. TESTER: Verify DUT remains in TIME-WAIT state for other cases 
    if(checkState(TIMEWAIT))
    {
        printf("\nTest passed\n");
        EndTest(socketId);
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in TIMEWAIT state\n");
        EndTest(socketId);
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_15_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_15 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: <wst> = SYN-RECEIVED
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_15_IT1()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 30000;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)sSeq);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Do not send any response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        //result ok
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }


    //4. TESTER: Verify that the DUT remains in <wst> state 
    if(checkState(SYNRCVD))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in SYN-RCVD state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_15_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_15 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: <wst> = ESTABLISHED
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_15_IT2()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 30000;
    moveServDUTToEstablished(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)sSeq);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Do not send any response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        //result ok
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }


    //4. TESTER: Verify that the DUT remains in <wst> state 
    if(checkState(ESTABLISHED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in ESTABLISHED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_15_IT3
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_15 iteration 3 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: <wst> = FINWAIT-1
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_15_IT3()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 30000;
    moveDUTToFINWAIT1(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)sSeq);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Do not send any response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        //result ok
    } 
    else 
    {
        int rSeqNumber  = (int)GetTCPField(&TCP_RP, TCP, TCP_SeqNumber);
        if (rSeqNumber != seqN)
        {
            printf("\nTest failed --> DUT responded while it shouldn't\n");
            TP_GenaralEndTest(NULL,0,(text){0});
            return 1;
        }
    }


    //4. TESTER: Verify that the DUT remains in <wst> state 
    if(checkState(FINWAIT1))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT1 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_15_IT4
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_15 iteration 4 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: <wst> = FINWAIT-2
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_15_IT4()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    int socketId;
    uint32 seqN, ackN;
    int sSeq = 30000;
    moveDUTToFINWAIT2(&socketId, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)sSeq);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Do not send any response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        //result ok
    } 
    else 
    {
        printf("\nTest failed --> DUT responded while it shouldn't\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }


    //4. TESTER: Verify that the DUT remains in <wst> state 
    if(checkState(FINWAIT2))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in FINWAIT2 state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_15_IT5
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_15 iteration 5 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: <wst> = CLOSE-WAIT
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_15_IT5()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Do not send any response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        //result ok
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }


    //4. TESTER: Verify that the DUT remains in <wst> state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSEWAIT)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_15_IT6
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_15 iteration 6 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: <wst> = CLOSING
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_15_IT6()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToClosing(&socketId, TCPConfig.DUT_Port , TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Do not send any response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        //result ok
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }


    //4. TESTER: Verify that the DUT remains in <wst> state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSING)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_15_IT7
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_15 iteration 7 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: <wst> = LAST-ACK
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_15_IT7()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToLASTACK(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Do not send any response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        //result ok
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }


    //4. TESTER: Verify that the DUT remains in <wst> state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == LASTACK)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_INVALID_15_IT8
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_INVALID_15 iteration 8 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: <wst> = TIME-WAIT 
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_INVALID_15_IT8()
{
    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    int socketId;
    uint32 seqN, ackN;
    moveDUTToTimeWait(&socketId, TCPConfig.DUT_Port , TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment with an unacceptable SEQ number
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Do not send any response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        //result ok
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }


    //4. TESTER: Verify that the DUT remains in <wst> state 
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == TIMEWAIT)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }
}