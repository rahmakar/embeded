#include "TCP_CALL_RECEIVE.h"


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CALL_RECEIVE_04_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP CALL RECEIVE 04  test IT 1.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CALL_RECEIVE_04_IT1()
{
    return 2;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CALL_RECEIVE_04_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP CALL RECEIVE 04  test IT 2.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CALL_RECEIVE_04_IT2()
{
    return 2;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CALL_RECEIVE_04_IT3
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP CALL RECEIVE 04  test IT 3.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CALL_RECEIVE_04_IT3()
{
    return 2;
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_CALL_RECEIVE_05
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP CALL RECEIVE 05  test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_CALL_RECEIVE_05()
{
    return 2;
}