#include "TCP_URGENT_PTR.h"


/***************************************************************************************************
*
*   FUNCTION NAME: TCP_URGENT_PTR_04
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP URGENT PTR 04 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_URGENT_PTR_04()
{
    return 2;
}
