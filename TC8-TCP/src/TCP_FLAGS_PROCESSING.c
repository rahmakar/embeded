#include "AbstractionAPI.h"
#include "TCPcommon.h"
#include "TCP_FLAGS_PROCESSING.h"

#include"TestabilityProtocol_api.h"
#include"TestabilityProtocol_Intern.h"

extern int state;
TP_ResultID_t TP_TcpListenAndAccept_cb_result;

void TP_TcpListenAndAccept_cb(TP_ResultID_t result)
{
    TP_TcpListenAndAccept_cb_result = result;
}

int TCP_FLAGS_PROCESSING_01()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move on to SYN-RCVD state initiated by active OPEN call
    uint32 seqN, ackN;
    moveDUTToSYNRCVD(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RESET segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);


    //3. TESTER: Cause DUT side app to issue a receive call
    TP_TcpListenAndAccept(TP_TcpListenAndAccept_cb, NULL, socketID, 5);
    sleep(3);


    //4. DUT: Returns error
    //5. TESTER: Verify that the DUT informs \"connection refused\" to the receiving application
    if(TP_TcpListenAndAccept_cb_result == RID_E_TCP_COR)
    {
        TP_GenaralEndTest(NULL, 0, (text){0});
        printf("\nTest passed\n");
        return 0;

    }
    else 
    {
        TP_GenaralEndTest(NULL, 0, (text){0});
        printf("\nTest failed --> DUT didn't inform \"connection refused\"\n");
        return 1;
    }


}

// 1. CASE: <wst> = SYN-RCVD 
int TCP_FLAGS_PROCESSING_02_IT1()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    uint32 seqN, ackN;
    moveDUTToSYNRCVD(&socketID, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);
    
    //2. TESTER: Send a RST segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);


    //3. DUT: Moves to CLOSED state
    //4. TESTER: Verify that the DUT moves on to CLOSED state
    TP_TcpGetState(TP_TcpGetState_cb, &socketID);
    if(TCP_Con_state == CLOSED)
    {
        TP_GenaralEndTest(NULL, 0, (text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }

}

//2. CASE: <wst> = ESTABLISHED
int TCP_FLAGS_PROCESSING_02_IT2()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);


    //3. DUT: Moves to CLOSED state
    //4. TESTER: Verify that the DUT moves on to CLOSED state
    TP_TcpGetState(TP_TcpGetState_cb, &socketID);
    if(TCP_Con_state == CLOSED)
    {
        TP_GenaralEndTest(NULL, 0, (text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL, 0, (text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }

}


//3. CASE: <wst> = FINWAIT-1
int TCP_FLAGS_PROCESSING_02_IT3()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    uint32 seqN, ackN;
    moveDUTToFINWAIT1(&socketID, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);


    //3. DUT: Moves to CLOSED state
    //4. TESTER: Verify that the DUT moves on to CLOSED state
    TP_TcpGetState(TP_TcpGetState_cb,&socketID);
    if(TCP_Con_state == CLOSED)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }

}


//4. CASE: <wst> = FINWAIT-2
int TCP_FLAGS_PROCESSING_02_IT4()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state
    uint32 seqN, ackN;
    moveDUTToFINWAIT2(&socketID, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);


    //3. DUT: Moves to CLOSED state
    //4. TESTER: Verify that the DUT moves on to CLOSED state
    TP_TcpGetState(TP_TcpGetState_cb,&socketID);
    if(TCP_Con_state == CLOSED)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }

}


//5. CASE: <wst> = CLOSE-WAIT
int TCP_FLAGS_PROCESSING_02_IT5()
{

    int socketID;

    
    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state 
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);


    //3. DUT: Moves to CLOSED state
    //4. TESTER: Verify that the DUT moves on to CLOSED state
    TP_TcpGetState(TP_TcpGetState_cb,&socketID);
    if(TCP_Con_state == CLOSED)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL, 0, (text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }

}

//1. CASE: <wst> = CLOSING 
int TCP_FLAGS_PROCESSING_03_IT1()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause the DUT to move on to <wst> state

    uint32 seqN, ackN;
    moveDUTToClosing(&socketID, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Goes to CLOSED state
    //4. TESTER: Verify that the DUT moves on to CLOSED state
    TP_TcpGetState(TP_TcpGetState_cb,&socketID);
    if(TCP_Con_state == CLOSED)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }
}

//2. CASE: <wst> = LAST-ACK
int TCP_FLAGS_PROCESSING_03_IT2()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause the DUT to move on to <wst> state 
    uint32 seqN, ackN;
    moveDUTToLASTACK(&socketID, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Goes to CLOSED state
    //4. TESTER: Verify that the DUT moves on to CLOSED state
    TP_TcpGetState(TP_TcpGetState_cb,&socketID);
    if(TCP_Con_state == CLOSED)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }
}


int TCP_FLAGS_PROCESSING_04()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause the DUT to move on to TIME-WAIT state
    uint32 seqN, ackN;
    moveDUTToTimeWait(&socketID, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a RST segment
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Goes to CLOSED state
    //4. TESTER: Verify that the DUT moves on to CLOSED state
    if(checkState(CLOSED))
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");
        return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed\n");
        return 1;
    }
}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_PROCESSING_05_IT1
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_PROCESSING_05 iteration 1 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: <stp> = SYN in window  
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_PROCESSING_05_IT1()
{

    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to SYN-RCVD state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a segment of type Stp
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send a RST segment 
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    { 
         int RSTFlag  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (RSTFlag == 1)
        {
            // result ok 
        } 
        else
        {
           
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> DUT response not ok\n");
            return 1;
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT do not send responding\n");
        return 1;

    }


    //4.TESTER: Verify that the receiving application receives the signal that \"connection reset\" has occurred from the remote site and the DUT moves on to CLOSED state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSED)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }

}

/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_PROCESSING_05_IT2
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_PROCESSING_05 iteration 2 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
*  -CASE: <stp> = SYN,ACK in window  
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_PROCESSING_05_IT2()
{

    //TP_OpenControlChannel();
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to SYN-RCVD state
    int socketId;
    uint32 seqN, ackN;
    moveDUTToSYNRCVD(&socketId, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a segment of type Stp
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagSYN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(seqN + 1));
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0);
    EditTCPField(&TCP_P, PAYLAOD, PAYLOAD_data, (void *)(intptr_t)"TEST TCP");
    EditTCPField(&TCP_P, IP, IP_TotalLength, (void *)(intptr_t)48);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Send a RST segment 
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    { 
         int RSTFlag  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (RSTFlag == 1)
        {
            // result ok 
        } 
        else
        {
           
            TP_GenaralEndTest(NULL,0,(text){0});
            printf("\nTest failed --> DUT response not ok\n");
            return 1;
        }
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT do not send responding\n");
        return 1;

    }


    //4.TESTER: Verify that the receiving application receives the signal that \"connection reset\" has occurred from the remote site and the DUT moves on to CLOSED state
    ip4addr ipDUT, ipTESTER;
    ipDUT.dataLength = 4;
    ipTESTER.dataLength = 4;
    stoiIP(TCPConfig.DUT_IP, ipDUT.Data);
    stoiIP(TCPConfig.DUT_IP, ipTESTER.Data);
    TP_TcpSocketState(TP_TcpGetState_cb, TCPConfig.DUT_Port, ipDUT, TCPConfig.Tester_Port, ipTESTER);
    if(state == CLOSED)
    {   
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");      
		return 0;
    }
    else
    {
         
		TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> state not ok\n");
        return 1;  

    }

}

int TCP_FLAGS_PROCESSING_06()
{
    
    int socketID;

    // start test
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause the DUT to move on to TIME-WAIT state
    uint32 seqN, ackN;
    moveDUTToTimeWait(&socketID, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send the last FIN once more
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN - 1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    
    //3. DUT: Send ACK for the retransmitted FIN
     Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0) 
    { 
        int rACK  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagACK);
        if (rACK == 1)
        {
              // Resukt OK
        } 
        else
        {
            printf ("\nTest failed --> DUT response is not ACK\n");
        }
    }
    else 
    {
	TP_GenaralEndTest(NULL,0,(text){0});
     
    printf("\nTest failed --> DUT not responding\n");
    return 1;
    }
    //4. TESTER: Wait for 1.5*MSL time
    sleep(1.5*TCPConfig.msl);

    //5. TESTER: Verify that the DUT remains in TIME-WAIT state
    TP_TcpGetState(TP_TcpGetState_cb,&socketID);
    if(TCP_Con_state == TIMEWAIT)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }
}


//1. CASE: <wst> = CLOSE-WAIT 
int TCP_FLAGS_PROCESSING_07_IT1()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause the DUT to move on to <wst> state
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a data segment with only URG flag set
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagURG, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);

    //3. DUT: Do not send any response  
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");
        return 0;
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }
}

//2. CASE: <wst> = CLOSING
int TCP_FLAGS_PROCESSING_07_IT2()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause the DUT to move on to <wst> state 
    uint32 seqN, ackN;
    moveDUTToClosing(&socketID, TCPConfig.DUT_Port, TCPConfig.Tester_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a data segment with only URG flag set
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagURG, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    
    //3. DUT: Do not send any response  
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0) { 
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");
        return 0;
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }
}

//3. CASE: <wst> = LAST-ACK
int TCP_FLAGS_PROCESSING_07_IT3()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause the DUT to move on to <wst> state
    uint32 seqN, ackN;
    moveDUTToLASTACK(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a data segment with only URG flag set
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagURG, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    
    //3. DUT: Do not send any response  
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0) { 
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");
        return 0;
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }
}

//4. CASE: <wst> = TIME-WAIT
int TCP_FLAGS_PROCESSING_07_IT4()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);
    
    //1. TESTER: Cause the DUT to move on to <wst> state
    uint32 seqN, ackN;
    moveDUTToTimeWait(&socketID, TCPConfig.DUT_Port, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a data segment with only URG flag set
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagURG, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)(ackN + 1));
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);
    
    //3. DUT: Do not send any response  
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0) { 
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");
        return 0;
    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }
}


//1. CASE: <wst> = CLOSED
int TCP_FLAGS_PROCESSING_08_IT1()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state

    //2. TESTER: Send a FIN
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x01);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);    

    //3. DUT: in SYN-SENT or LISTEN state : Do not send any response or retransmit SYN In CLOSED state : send a RST segment
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length != 0)
    { 
        int rRST  = (int)GetTCPField(&TCP_RP, TCP, TCP_FlagRST);
        if (rRST == 1)
        {
            // result ok
        } 
        else
        {
            printf ("\nTest failed --> DUT response is not SYN ACK\n");
        }
    } 
    else 
    {
	TP_GenaralEndTest(NULL,0,(text){0});
    printf("\nTest failed --> DUT not responding\n");
    return 1;

    }
    //4. TESTER: Verify that the DUT remains in <wst> state
    if(checkState(CLOSED))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in CLOSED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}

//2. CASE: <wst> = LISTEN
int TCP_FLAGS_PROCESSING_08_IT2()
{
    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state
    moveDUTToListen(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon);

    //2. TESTER: Send a FIN
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x01);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);   

    //3. DUT: in SYN-SENT or LISTEN state : Do not send any response or retransmit SYN In CLOSED state : send a RST segment
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        // DUT do not send any response
    } 
    else 
    {
	    TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest failed --> DUT send responding\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in <wst> state
    if(checkState(LISTEN))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in CLOSED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}
//3. CASE: <wst> = SYN-SENT 
int TCP_FLAGS_PROCESSING_08_IT3()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state
    moveDUTToSYNSENT(&socketID, TCPConfig.DUT_Port, TCPConfig.DUT_Port, TCPConfig.Maxcon, (uint32*)NULL, (uint32*)NULL);

    //2. TESTER: Send a FIN
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagFIN, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x01);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);    
    //3. DUT: in SYN-SENT or LISTEN state : Do not send any response or retransmit SYN In CLOSED state : send a RST segment
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0)
    { 
        // DUT do not send any response
    } 
    else 
    {
	    TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT responded while it shouldn't\n");
        return 1;
    }

    //4. TESTER: Verify that the DUT remains in <wst> state
    if(checkState(SYNSENT))
    {
        printf("\nTest passed\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 0;
    }
    else
    {
        printf("\nTest failed --> DUT is not in CLOSED state\n");
        TP_GenaralEndTest(NULL,0,(text){0});
        return 1;
    }
}


//1. CASE: <wst> = CLOSE-WAIT
int TCP_FLAGS_PROCESSING_09_IT1()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state
    uint32 seqN, ackN;
    moveDUTToCloseWait(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a FIN
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x00);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0x00);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);    

    //3. DUT: Do not change state
    //4. TESTER: Verify that the DUT remains in <wst> state
    TP_TcpGetState(TP_TcpGetState_cb,&socketID);
    if(TCP_Con_state == CLOSEWAIT)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }
}

//2. CASE: <wst> = CLOSING
int TCP_FLAGS_PROCESSING_09_IT2()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state
    uint32 seqN, ackN;
    moveDUTToClosing(&socketID, TCPConfig.DUT_Port, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a FIN
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x00);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0x00);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);    

    //3. DUT: Do not change state
    //4. TESTER: Verify that the DUT remains in <wst> state
    TP_TcpGetState(TP_TcpGetState_cb,&socketID);
    if(TCP_Con_state == CLOSING)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }
}

//3. CASE: <wst> = LAST-ACK
int TCP_FLAGS_PROCESSING_09_IT3()
{

    int socketID;

    // start test
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause the DUT to move on to <wst> state
    uint32 seqN, ackN;
    moveDUTToLASTACK(&socketID, TCPConfig.DUT_Port, TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send a FIN
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagRST, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)0x00);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)0x00);
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);    

    //3. DUT: Do not change state
    //4. TESTER: Verify that the DUT remains in <wst> state
    TP_TcpGetState(TP_TcpGetState_cb,&socketID);
    if(TCP_Con_state == LASTACK)
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest passed\n");
		return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0});
         
        printf("\nTest failed\n");
		return 1;  

    }
}
/***************************************************************************************************
*
*   FUNCTION NAME: TCP_FLAGS_PROCESSING_10
*
***************************************************************************************************/
/**
* @brief
*  Performs TCP_FLAGS_PROCESSING_10 test.
*
* @return int : 0: The state progression was successful | 1: The state progression didn't go as expected.
*
* @note
* -
*
* @warning
* -
*
***************************************************************************************************/
int TCP_FLAGS_PROCESSING_10()
{
    return 2;
}

int TCP_FLAGS_PROCESSING_11()
{

    int socketID;

    //start test    
    TP_GenaralStartTest(NULL);

    //1. TESTER: Cause DUT to move to ESTABLISHED state
    uint32 seqN, ackN;
    moveServDUTToEstablished(&socketID, TCPConfig.DUT_Port , TCPConfig.Maxcon, &seqN, &ackN);

    //2. TESTER: Send the last ACK once more
    TCP_Packet TCP_P = CreateTCP();
    EditTCPField(&TCP_P, TCP, TCP_FlagACK, (void *)(intptr_t)1);
    EditTCPField(&TCP_P, TCP, TCP_SrcPort, (void *)(intptr_t)TCPConfig.Tester_Port);
    EditTCPField(&TCP_P, TCP, TCP_DstPort, (void *)(intptr_t)TCPConfig.DUT_Port);
    EditTCPField(&TCP_P, TCP, TCP_SeqNumber, (void *)(intptr_t)seqN);
    EditTCPField(&TCP_P, TCP, TCP_AckNumber, (void *)(intptr_t)(ackN - 1));
    TCP_Compute_checksum(&TCP_P);
    SendTCP(TCP_P);    

    //3. DUT: Do not send any response
    Packet_filter f;
    strcpy(f.SrcAdd, TCPConfig.DUT_IP);
    strcpy(f.dstAdd, TCPConfig.TESTER_IP);
    f.Srcport = TCPConfig.DUT_Port;
    f.Dstport = TCPConfig.Tester_Port;
    // Sniffing and verifying DUT response
    TCP_Packet TCP_RP = ListenTCP(f);
    if (TCP_RP.length == 0) { 
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");
        return 0;    } 
    else 
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        TP_CloseControlChannel();
        printf("\nTest failed --> DUT send responding\n");
        return 1;

    }

    //4. TESTER: Verify that the DUT remains in ESTABLISHED state
    if(checkState(ESTABLISHED))
    {
        TP_GenaralEndTest(NULL,0,(text){0});
        printf("\nTest passed\n");
        return 0;
    }
    else
    {
        TP_GenaralEndTest(NULL,0,(text){0}); 
        printf("\nTest failed\n");
        return 1;
    }

}